<?php

function read_get($name) {
	if (isset($_GET[$name])) {
		return $_GET[$name];
	}
	return null;
}

function read_get_string($name) {
	return trim(read_get($name));
}

function read_get_integer($name) {
	return intval(read_get_string($name));
}

?>
