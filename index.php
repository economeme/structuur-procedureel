<?php

	// Disclaimer: Dit voorbeeld is tijdens een les gemaakt en niet perfect.
	// Het gaat er om dat je een idee krijgt hoe je een php-project enige structuur kunt geven.
	// Het gebruik van OOP is bewust vermeden (behalve voor mysqli), ook al worden veel dingen hierdoor gemakkelijker.
	// 17 okt 2018 MESM

	require("config.php");
	require("include/db.php");
	require("include/forms.php");

	// Een eenvoudige vorm van 'routing'
	$page=read_get_string('page');
	switch($page) {
		case 'producten':
			require "pages/page_producten.php";
			break;
		case 'overons':
			require "pages/page_overons.php";
			break;
		default:
			require "pages/page_home.php";
	}
	$html_content=get_html_page();

	// User en login message in header. Hier kun je ook authenticatie starten
	// (in dit eenvoudige voorbeeld niet geïmplementeerd, zet alles hiervoor in users.php en vooral niet in index.php)
	require("include/users.php");
	if (user_logged_in()) {
		$html_header_loginstatus = "Ingelogd als ".user_get_username();
	} else {
		$html_header_loginstatus = "Niet ingelogd";
	}

echo <<<EOT
<!DOCTYPE html>
<html lang="nl">
	<head>
		<title>Aanmeldformulier</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="project.css">
	</head>
	<body>
		<div id="header">
			<h1>Mijn plantenzaak</h1>
			<p>
				<a href="?page=home">Home</a>
				<a href="?page=producten">Producten</a>
				<a href="?page=overons">Over ons</a>
				<span style="margin-left: 30px;">{$html_header_loginstatus}</span>
			</p>
		</div>

		<div id="content">
			{$html_content}
		</div>

		<div id="footer">
			&copy; 2018 Mijn plantenzaak
		</div>

	</body>
</html>
EOT;
?>
