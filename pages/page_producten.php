<?php

function get_html_page() {
	global $mysqli;
	$html=<<<EOT
<p>Zoeken: <form method="GET">
<input name="zoekveld" /> 
<input type="hidden" name="page" value="producten" />
<input type="submit" value="Zoeken" /></form></p>
EOT;

	$zoekveld=read_get_string('zoekveld');
	if (empty($zoekveld)) {
		$query="SELECT plantnaam, prijs FROM plant ORDER BY plantnaam LIMIT 10";
	} else {
		$query="SELECT plantnaam, prijs FROM plant WHERE plantnaam LIKE '%".$mysqli->real_escape_string($zoekveld)."%' ORDER BY plantnaam LIMIT 10";
	}

	$res=$mysqli->query($query);
	if ($res->num_rows==0) {
		$html.="<p>Geen producten gevonden.</p>";
	} else {
		while ($row=$res->fetch_row()) {
			// TODO: Prijs fatsoenlijk weergeven!
			$html.=<<<EOT
	<div class="product">
		<h2>{$row[0]}</h2>
		Prijs: € {$row[1]}
	</div>
EOT;
		}
	}
	return $html;
}

?>
